(function() {
    'use strict';

    angular
        .module('app.services')
        .factory('GetContactsService', GetContactsService);

    GetContactsService.$inject = ['$q'];

    function GetContactsService($q) {

        return {
            invoke: invoke
        };

        /* */

        function invoke(contactName) {
            var deferred = $q.defer();

            Visualforce.remoting.Manager.invokeAction(
                'CurrencyLayerController.getContacts',
                contactName, 
                function(result, event){
                    if (event.status) {
                        deferred.resolve(result);
                    } else if (event.type === 'exception') {
                        deferred.reject(result);
                    } else {
                        deferred.reject(result);
                    }
                }, 
                {escape: true}
            );

            return deferred.promise;
        }
    }
})();