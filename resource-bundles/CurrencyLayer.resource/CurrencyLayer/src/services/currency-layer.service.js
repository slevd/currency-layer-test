(function() {
    'use strict';

    angular
        .module('app.services')
        .factory('CurrencyLayerService', CurrencyLayerService);

    CurrencyLayerService.$inject = ['$q', '$http'];

    function CurrencyLayerService($q, $http) {

        return {
            invoke: invoke
        };

        /* */

        function invoke(endpoint, source, exchangeDate) {
            var deferred = $q.defer();
                    
            // set your access key
            var access_key = 'dcd47255cfda5c2261e0911437f0b321';

            var deferred = $q.defer();

            source = (source) ? source : '';
            exchangeDate = (exchangeDate) ? exchangeDate : '';

            Visualforce.remoting.Manager.invokeAction(
                'CurrencyLayerController.getCurrencyExchanges',
                endpoint,
                access_key,
                source,
                exchangeDate,
                function(result, event){
                    if (event.status) {
                        deferred.resolve(JSON.parse(result.replace(/&quot;/g,'"')));
                    } else if (event.type === 'exception') {
                        deferred.reject(JSON.parse(result));
                    } else {
                        deferred.reject(JSON.parse(result));
                    }
                }, 
                {escape: true}
            );

            return deferred.promise;
        }
    }
})();