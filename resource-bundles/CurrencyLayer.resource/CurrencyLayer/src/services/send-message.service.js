(function() {
    'use strict';

    angular
        .module('app.services')
        .factory('SendMsgService', SendMsgService);

    SendMsgService.$inject = ['$q'];

    function SendMsgService($q) {

        return {
            invoke: invoke
        };

        /* */

        function invoke(contactId, currencyExchange, source, exchangeDate) {
            var deferred = $q.defer();

            Visualforce.remoting.Manager.invokeAction(
                'CurrencyLayerController.sendEmail',
                contactId,
                currencyExchange,
                source,
                exchangeDate,
                function(result, event){
                    if (event.status) {
                        deferred.resolve(result);
                    } else if (event.type === 'exception') {
                        deferred.reject(result);
                    } else {
                        deferred.reject(result);
                    }
                }, 
                {escape: true}
            );

            return deferred.promise;
        }
    }
})();