(function() {
	'use strict';

	angular
		.module('app.datepicker')
		.component('datepicker', {
			templateUrl: host + '/CurrencyLayer/src/datepicker/datepicker.html',
			controller: datepickerCtrl,
			controllerAs: 'vm',
			bindings: {
				onSelect: '&',
				datepickerId: "@"
			}
		});

		datepickerCtrl.$inject = ['$timeout'];

		function datepickerCtrl($timeout) {
            
            var vm = this;

            var $j = jQuery.noConflict();
                //Assigning SLDS Static Resource Path To A Variable To Use It With ALJS Initialization
                
            var assetsLoc = assetsLocation;

            var eventDateValue = ''; 
            
            //Override Common Settings with SLDS Static Resource Path (Optional)       
            $j.aljsInit ({
                assetsLocation: assetsLocation, // Specifies the directory containing SLDS's assets directory 
                scoped: true               // Specifies whether or not the page is scoped using the slds class
            });
                
                
            //Initializing Datepicker 
            vm.$onInit = function() {
                $timeout( function() {   
                    $j('#' + vm.datepickerId).datepicker({
                        initDate: moment(),   // Assign Today's Date
                        format: 'YYYY-MM-DD', // Specify the Date Format Of Datepicker Input Field (YYYY-MM-DD,YYYY/MM/DD ,DD/MM/YYYY ....etc)
                        // onChange function to change the value once it is selected
                        onChange: function (datepicker) {
                            // Specify the format      
                            eventDateValue = moment(datepicker.selectedFullDate._d).format('YYYY-MM-DD');  
                            vm.onSelect({date: eventDateValue});
                        }
                    });
                });
            }
        }
})();