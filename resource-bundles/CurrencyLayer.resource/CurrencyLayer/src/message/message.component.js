(function() {
	'use strict';

	angular
		.module('app.message')
		.component('message', {
			templateUrl: host + '/CurrencyLayer/src/message/message.html',
			controller: messageCtrl,
			controllerAs: 'vm',
			bindings: {
                onClose: '&',
				messageText: "<",
				messageType: "<"
			}
		});

		messageCtrl.$inject = [];

		function messageCtrl() {
            var vm = this;

            vm.hideMessage = hideMessage;

            function hideMessage () {
                vm.onClose();
            }
           
        }
})();