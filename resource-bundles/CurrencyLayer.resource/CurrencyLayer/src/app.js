(function() {
    'use strict';

    angular.module('app', [
        'app.currencyLayer',
        'app.currencyLayerTable',
        'app.picklist',
        'app.lookup',
        'app.datepicker',
        'app.message',        
        'app.services'
    ]);
})();