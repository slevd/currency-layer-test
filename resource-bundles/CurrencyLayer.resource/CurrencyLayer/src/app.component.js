(function() {
	'use strict';

	angular
		.module('app')
		.component('app', {
			templateUrl: host + '/CurrencyLayer/src/app.html',
			controller: AppCtrl,
			controllerAs: 'vm'
		});

		AppCtrl.$inject = [];

		function AppCtrl() {}
})();