(function() {
	'use strict';

	angular
		.module('app.picklist')
		.component('picklist', {
			templateUrl: host + '/CurrencyLayer/src/picklist/picklist.html',
			controller: picklistCtrl,
			controllerAs: 'vm',
			bindings: {
                onSelect: '&',
                picklistArr: '<',
                assistiveText: '@',
                picklistLabel: '@'

			}
		});

		picklistCtrl.$inject = ['$timeout'];

		function picklistCtrl($timeout) {
            var vm = this;

            var $j = jQuery.noConflict();

            //Override Common Settings with SLDS Static Resource Path (Optional)       
            $j.aljsInit ({
                assetsLocation: assetsLocation, // Specifies the directory containing SLDS's assets directory 
                scoped: true               // Specifies whether or not the page is scoped using the slds class
            });

            vm.$onChanges = function(context) {              
                renderPicklist();
            }
            
            function renderPicklist() {
                $timeout( function() {
                    $j('[data-aljs="picklist"]').picklist({
                        onChange: function(obj) {
                            vm.onSelect({source: obj.value});
                        }
                    });
                });
            }
                     
        }
})();