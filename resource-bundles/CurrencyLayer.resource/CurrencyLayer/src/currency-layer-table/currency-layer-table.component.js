(function() {
	'use strict';

	angular
		.module('app.currencyLayerTable')
		.component('currencyLayerTable', {
			templateUrl: host + '/CurrencyLayer/src/currency-layer-table/currency-layer-table.html',
			controller: currencyLayerTableCtrl,
			controllerAs: 'vm',
			bindings: {
				currencyExchange: '<',
				numberOfTables: "@"
			}
		});

		currencyLayerTableCtrl.$inject = [];

		function currencyLayerTableCtrl() {
            var vm = this;

			vm.currencyExchangeTables = [];

            vm.$onChanges = function(context) {   
                renderTable();
            }

            function renderTable() {

                var currencyExchangeTables = [];
                var size = vm.currencyExchange.length / vm.numberOfTables;

                while (vm.currencyExchange.length > 0) {
                    currencyExchangeTables.push(vm.currencyExchange.splice(0, Math.ceil(size)));
                }
                
                vm.currencyExchangeTables = currencyExchangeTables;
            }
		}
})();