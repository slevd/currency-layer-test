(function() {
	'use strict';

	angular
		.module('app.currencyLayer')
		.component('currencyLayer', {
			templateUrl: host + '/CurrencyLayer/src/currency-layer/currency-layer.html',
			controller: currencyLayerCtrl,
			controllerAs: 'vm'
		});

		currencyLayerCtrl.$inject = ['CurrencyLayerService', 'SendMsgService'];

		function currencyLayerCtrl(CurrencyLayerService, SendMsgService) {
			var vm = this;

			vm.currencyExchange = [];

			vm.currencyExchangeSources = [];

			vm.currencyExchangeJSON = '';

			vm.selectedContactId;
			vm.selectedDate;
			vm.selectedCurrency = 'USD';
			vm.errorMessage;
			vm.isShowMessage = false;
			vm.showTable = false;
			vm.showPicklist = false;

			vm.setSelectedContact = setSelectedContact;
			vm.setSelectedDate = setSelectedDate;
			vm.setSelectedSource = setSelectedSource;
			vm.getCurrencyExchange = getCurrencyExchange;
			vm.getCurrencyExchangeHandler = getCurrencyExchangeHandler;
			vm.sendMsg = sendMsg;
			vm.hideMessage = hideMessage;
			vm.showMessage = showMessage;
			
			getCurrencyExchange('live', 'USD', '');

			function setSelectedSource(source) {
				vm.selectedCurrency = source;
            }

			function setSelectedContact(contactId) {
				vm.selectedContactId = contactId;
            }

			function setSelectedDate(date) {
				vm.selectedDate = date;
            }

			function hideMessage() {
				vm.isShowMessage = false;
            }

			function showMessage(messageText, messageType) {
				vm.messageText = messageText;
				vm.messageType = messageType;
				vm.isShowMessage = true;
            }

			function getCurrencyExchangeHandler() {
				
				var endpoint;
				var selectedDateFormatted = new Date(vm.selectedDate.replace(/-/g,'/'));  
				var today = new Date();
				var todayFormatted = new Date();
				var oldDate = new Date(today.setFullYear(today.getFullYear() - 16));

				if (selectedDateFormatted < oldDate) {
					showMessage('Date should not be later then 16 years ago!', 'slds-theme--error');
					return;
				}
				else if (selectedDateFormatted < todayFormatted) {
					endpoint = 'historical';
				}
				else if (selectedDateFormatted.getTime() == todayFormatted.getTime()) {
					endpoint = 'live';
				}
				else if (selectedDateFormatted > todayFormatted) {
					showMessage('Date should not be in future!', 'slds-theme--error');
					return;
				}

				getCurrencyExchange(endpoint, vm.selectedCurrency, vm.selectedDate);
			}

			function sendMsg() {

				SendMsgService.invoke(vm.selectedContactId, vm.currencyExchangeJSON, vm.selectedCurrency, vm.selectedDate).then(
					function (response) {
						if (response.indexOf('Success') > -1) {
							showMessage('Message has been sended sucessfully!', 'slds-theme--success');
						}
						else {
							showMessage(response, 'slds-theme--error');
						}
					}
				);
			}

			function getCurrencyExchange(endpoint, source, date) {
				CurrencyLayerService.invoke(endpoint, source, date).then(function (response) {
					console.log(response);
					if (response.error) {
						showMessage(response.error.info, 'slds-theme--error');
						vm.showPicklist = false;
						vm.selectedCurrency = 'USD';
						return;
					}

					var quotes = response.quotes;

					vm.currencyExchange = [];

					vm.currencyExchangeSources = [];

					Object.keys(quotes).forEach(function(objectKey, index) {
						vm.currencyExchange.push({
							code: objectKey.split(vm.selectedCurrency)[1],
							exchange: quotes[objectKey]
						});
						vm.currencyExchangeSources.push(
							objectKey.split(vm.selectedCurrency)[1]
						);
					});
					vm.currencyExchangeJSON = JSON.stringify(vm.currencyExchange);

					vm.showTable = true;
					vm.showPicklist = true;
					hideMessage();
				});
			}
		}
})();