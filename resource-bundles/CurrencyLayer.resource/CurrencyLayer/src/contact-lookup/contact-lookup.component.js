(function() {
	'use strict';

	angular
		.module('app.lookup')
		.component('lookup', {
			templateUrl: host + '/CurrencyLayer/src/contact-lookup/contact-lookup.html',
			controller: lookupCtrl,
			controllerAs: 'vm',
			bindings: {
				onSelect: '&',
				lookupId: "@"
			}
		});

		lookupCtrl.$inject = ['GetContactsService', '$timeout'];

		function lookupCtrl(GetContactsService, $timeout) {
            var vm = this;

			var $j = jQuery.noConflict();
			//Override Common Settings with SLDS Static Resource Path (Optional)       
            $j.aljsInit ({
                assetsLocation: assetsLocation, // Specifies the directory containing SLDS's assets directory 
                scoped: true               // Specifies whether or not the page is scoped using the slds class
            });
            
            vm.getContacts = getContacts;

			
			vm.$onInit = function() {
				$timeout( function() {
					$j('#' + vm.lookupId).lookup({
						filledSearchTermQuery: function(searchTerm, callback) { 
							getContacts(searchTerm, callback);
						},
						emptySearchTermQuery: function(callback) {
							getContacts('', callback);
						},
						onChange: function(currentSelection, isAdd) {
							vm.onSelect({contactId: (isAdd) ? currentSelection.id : null});
						}
					});
				})		
			}
			
            function getContacts(contactName, callback) {
				var contacts = [];
                GetContactsService.invoke(contactName).then(function (response) {                    
					response.forEach(function(element, index, array){
						contacts.push({
							id: element.Id,
							label: element.Name,
							metaLabel: element.Email
						})
					});
					callback(contacts);
                });
            }
		}
})();