public with sharing class CurrencyPDFController {
    
    public List<CurrencyExchangeWrap> currencyExchanges = new List<CurrencyExchangeWrap>();
    public Date dateExchange { get; set; }
    public String source { get; set; }
    public Currency_Exchange__c currencyExchangeObj { get; set; }
    public String currencyExchangeId { get; set; }

    public Date getDateExchange() {

        this.dateExchange = this.currencyExchangeObj.Date_Exchange__c;

		return this.dateExchange;
	}

    public String getSource() {

        this.source = this.currencyExchangeObj.Currency__c;        

		return this.source;
	}

    public List<CurrencyExchangeWrap> getCurrencyExchanges() {

        this.currencyExchangeObj = [
            SELECT Id, Currency__c, Date_Exchange__c, Contact__c, Currency_Exchange_JSON__c
            FROM Currency_Exchange__c
            WHERE Id =: currencyExchangeId
        ][0];

        this.currencyExchanges = (List<CurrencyExchangeWrap>)JSON.deserialize(
            this.currencyExchangeObj.Currency_Exchange_JSON__c, List<CurrencyExchangeWrap>.class
        );

		return this.currencyExchanges;
	}

    public class CurrencyExchangeWrap {
        public String code { get; set; }
        public String exchange { get; set; }
    }

}