@isTest
private class CurrencyLayerControllerTest {

    static testMethod void getContactsTest() {
        Contact cont = CurrencyLayerControllerTest.createContact();
        List<Contact> contactactsClsr;
        List<Contact> contactactsWithNameClsr;

        Test.startTest();
            contactactsClsr = CurrencyLayerController.getContacts('');
            contactactsWithNameClsr = CurrencyLayerController.getContacts(cont.LastName);
        Test.stopTest();

        System.assertEquals(1, contactactsWithNameClsr.size());
        System.assertEquals(1, contactactsClsr.size());
    }

    static testMethod void sendEmailTest() {
        Contact cont = CurrencyLayerControllerTest.createContact();
        String currencyExchange = '[{"code": "ASD", "exchange": "1.1"}]';
        String source = 'USD';
        String exchangeDateValid = '2017-02-02';
        String exchangeDateInvalid = 'invalid-invalid';
        String resultSuccess;
        String resultError;

        Test.startTest();
            resultSuccess = CurrencyLayerController.sendEmail(cont.Id, currencyExchange, source, exchangeDateValid);
            resultError = CurrencyLayerController.sendEmail(cont.Id, currencyExchange, source, exchangeDateInvalid);
        Test.stopTest();

        System.assertEquals('Success', resultSuccess);
        System.assertEquals(1, [SELECT Id FROM Currency_Exchange__c].size());
        System.assertEquals('Invalid integer: invalid', resultError);
    }

    static testMethod void getCurrencyExchangesTest() {

        Test.setMock(HttpCalloutMock.class, new CurrencyLayerMockResponse());

        Contact cont = CurrencyLayerControllerTest.createContact();
        String source = 'USD';
        String exchangeDate = '2017-02-02';
        String endpoint = '2017-02-02';
        String access_key = 'dcd47255cfda5c2261e0911437f0b321';
        String resultSuccess;

        Test.startTest();
            resultSuccess = CurrencyLayerController.getCurrencyExchanges(endpoint, access_key, source, exchangeDate);
        Test.stopTest();

        System.assertEquals('[{"code": "ASD", "exchange": "1.1"}]', resultSuccess);
    }

    private static Contact createContact() {
        Contact cont = new Contact(
			LastName = 'Test',
			Email = 'testtest@test.com'
		);
		insert cont;
        return cont;
    }
}