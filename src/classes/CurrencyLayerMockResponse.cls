@isTest
public with sharing class CurrencyLayerMockResponse implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse request = new HttpResponse();
        request.setBody('[{"code": "ASD", "exchange": "1.1"}]');
        request.setStatus('OK');
        request.setStatusCode(200);
        System.assertEquals('OK', request.getStatus());
        return request;
    }
}