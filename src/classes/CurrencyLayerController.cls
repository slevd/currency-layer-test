public with sharing class CurrencyLayerController {

    @RemoteAction
    public static List<Contact> getContacts(String contactName) {

        List<Contact> contacts = new List<Contact>();

        if(String.isBlank(contactName)){
            contacts = [SELECT Id, Name, Email FROM Contact LIMIT 15];
        }
        else {
            contacts = [SELECT Id, Name, Email FROM Contact WHERE Name LIKE: '%' + contactName + '%' LIMIT 15];
        }
        return contacts;
    }

    @RemoteAction
    public static String sendEmail(String contactId, String currencyExchange, String source, String exchangeDate) {
        try {
            Date exchangeDateFormatted = Date.newInstance(Integer.valueOf(exchangeDate.split('-')[0]),
             Integer.valueOf(exchangeDate.split('-')[1]), Integer.valueOf(exchangeDate.split('-')[2]));
            
            Currency_Exchange__c currencyExchangeObj = new Currency_Exchange__c(
                Currency__c = source,
                Date_Exchange__c = exchangeDateFormatted,
                Contact__c = contactId,
                Currency_Exchange_JSON__c = currencyExchange
            );
            insert currencyExchangeObj;
            
            return 'Success';
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @RemoteAction
    public static String getCurrencyExchanges(String endpoint, String access_key, String source, String exchangeDate) {
        HttpRequest req = new HttpRequest();

        req.setMethod('GET');
        req.setEndpoint('callout:CurrencyLayerService' + '/' + endpoint + '?access_key=' + access_key + 
                         ((String.isNotBlank(source)) ? '&source=' + source : '') + 
                         ((String.isNotBlank(exchangeDate)) ? '&date=' + exchangeDate : ''));
        req.setTimeout(120000);

        Http binding = new Http();
        HttpResponse response = binding.send(req);
 
        return response.getBody();
    }
}