@isTest
private class CurrencyPDFControllerTest {

    static testMethod void generatePdfTest() {

        Contact cont = CurrencyPDFControllerTest.createContact();

        Currency_Exchange__c currencyExchangeObj = new Currency_Exchange__c(
            Currency__c = 'USD',
            Date_Exchange__c = Date.today(),
            Contact__c = cont.Id,
            Currency_Exchange_JSON__c = '[{"code": "ASD", "exchange": "1.1"}]'
        );
        insert currencyExchangeObj;

        Date dateExchange;
        String source;
        List<CurrencyPDFController.CurrencyExchangeWrap> currencyExchanges;
        
        Test.startTest();
            CurrencyPDFController currencyPDFCtrl = new CurrencyPDFController();

			currencyPDFCtrl.currencyExchangeId = currencyExchangeObj.Id;

            currencyExchanges = currencyPDFCtrl.getCurrencyExchanges();
            dateExchange = currencyPDFCtrl.getDateExchange();
            source = currencyPDFCtrl.getSource();
		Test.stopTest();

		System.assertEquals(Date.today(), dateExchange);
		System.assertEquals('USD', source);
        System.assertEquals(1, currencyExchanges.size());
    }

    private static Contact createContact() {
        Contact cont = new Contact(
			LastName = 'Test',
			Email = 'testtest@test.com'
		);
		insert cont;
        return cont;
    }
}